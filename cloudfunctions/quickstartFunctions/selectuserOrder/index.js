const cloud = require('wx-server-sdk')

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database();
const $ = db.command.aggregate;

// 查询数据库集合云函数入口函数
exports.main = async (event, context) => {
  // 返回数据库查询结果
  return await db.collection('userOrder').aggregate().sort({
    date: -1
  }).match({
    status: 1
  }).project({
    _id: 0,
    month: $.substr(['$timeEnd', 4, 2]),
    day: $.substr(['$timeEnd', 6, 2]),
    name: 1,
    subject: 1,
    totalFee: 1
  }).limit(50).end()
}