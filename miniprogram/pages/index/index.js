//index.js
const app = getApp()
const {
  envList
} = require('../../envList.js')

Page({
  data: {
    showInfo: 0,
    value: 0,
    list: [{
      id: 'about',
      name: '关于我们',
      open: false,
      url: '/pages/about/about'
    }, ]
  },
  // 右上角分享
  onShareAppMessage: function (res) {
    return {
      title: '留云精舍随喜功德登记系统',
      path: '/pages/index/index',
      imageUrl: 'cloud://cloud1-2gwos8o132309cdb.636c-cloud1-2gwos8o132309cdb-1307075212/share01.jpg'
    }
  },
  // 分享到朋友圈
  onShareTimeline() {
    return {
      title: '留云精舍随喜功德登记系统',
    }
  },
  onLoad(options) {
    var that = this;
    that.setData({
      envId: options.envId
    });
    wx.showLoading({
      title: '加载中...',
    })
    that.getSwitch();
  },
  // 获取switch信息
  getSwitch: function () {
    var that = this;
    wx.cloud.callFunction({
      name: 'quickstartFunctions',
      data: {
        type: 'getSwitch'
      }
    }).then((resp) => {
      that.setData({
        showInfo: resp.result.data[0].status
      });
      wx.hideLoading();
      if (that.data.showInfo == 1) {
        that.getSubjectList();
        that.getuserOrder();
      }
    })
  },
  // 获取项目列表
  getSubjectList: function () {
    var that = this;
    wx.cloud.callFunction({
      name: 'quickstartFunctions',
      data: {
        type: 'selectSubject'
      }
    }).then((resp) => {
      that.setData({
        subject_array: resp.result.data
      })
    })
  },
  // 获取功德榜信息
  getuserOrder: function () {
    var that = this;
    wx.cloud.callFunction({
      name: 'quickstartFunctions',
      data: {
        type: 'selectuserOrder'
      }
    }).then((resp) => {
      that.setData({
        order_array: resp.result.list,
        rowup: resp.result.list.length * 25 * 2
      });
    })
  },
  // 调起微信支付
  wxPay: function () {
    wx.showLoading({
      title: '系统正在处理',
    })
    var that = this;
    wx.cloud.callFunction({
      name: 'wxPay',
      data: {
        goodsName: that.data.subject_name,
        totalFee: that.data.money,
        name: that.data.name,
        envId: envList[0].envId
      },
      success(res) {
        wx.hideLoading();
        console.log(res);
        that.setData({
          result_array: res.result
        })
        const payment = res.result.payment;
        wx.showLoading({
          title: '系统正在处理',
        });
        wx.requestPayment({
          ...payment,
          success(res) {
            console.error('pay success', res);
            wx.hideLoading();
            wx.redirectTo({
              url: '../message/index?success=true&name=' + that.data.name + '&subject_name=' + that.data.subject_name + '&money=' + that.data.money,
            })
          },
          fail(err) {
            wx.hideLoading();
            console.error('pay fail', err);
            wx.navigateTo({
              url: '../message/index?success=false&name=' + that.data.name + '&subject_name=' + that.data.subject_name + '&money=' + that.data.money,
            })
          }
        })
      },
      fail: console.error,
    })
  },
  // 获取捐赠项目名称
  bindSubjectChange: function (e) {
    this.setData({
      value: e.detail.value,
      subject_name: this.data.subject_array[e.detail.value].name
    })
  },
  // 获取捐赠金额
  moneyInput: function (e) {
    this.setData({
      money: e.detail.value * 100 // 换算成单位：分
    })
  },
  // 获取施主姓名
  nameInput: function (e) {
    this.setData({
      name: e.detail.value
    })
  },
  // 验证信息并支付
  submitInfo: function () {
    var that = this;
    if (that.data.money == null || that.data.name == null || that.data.subject_name == null) {
      if (that.data.subject_name == null) {
        wx.showModal({
          title: '提示',
          content: '捐赠项目必须选择',
          showCancel: false
        })
      } else if (that.data.money == null) {
        wx.showModal({
          title: '提示',
          content: '捐赠金额不能为空',
          showCancel: false
        })
      } else if (that.data.name == null) {
        wx.showModal({
          title: '提示',
          content: '捐赠者不能为空',
          showCancel: false
        })
      }
    } else {
      // 内容安全过滤
      wx.cloud.callFunction({
        name: 'msgCheck',
        data: {
          content: that.data.name
        },
        success(res) {
          if (res.result.errCode == 87014) {
            wx.showToast({
              icon: 'error',
              title: '文字内容违规',
              duration: 3000
            })
          } else {
            // 调用微信支付
            that.wxPay();
          }
        }
      })
    }
  }
})