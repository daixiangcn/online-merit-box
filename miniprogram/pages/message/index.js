// pages/message/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    money: 0,
    success: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    that.setData({
      success: options.success,
      name: options.name,
      subject_name: options.subject_name,
      money: options.money/100 // 单位换成元
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  backHome:function(){
    if(this.data.success == 'true'){
      wx.redirectTo({
        url: '../index/index',
      })
    }else{
      wx.navigateBack();
    }
  }
})