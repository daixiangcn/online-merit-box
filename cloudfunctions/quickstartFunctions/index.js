const getSwitch = require('./getSwitch/index')
const getImage = require('./getImage/index')
const createCollection = require('./createCollection/index')
const selectuserOrder = require('./selectuserOrder/index')
const updateRecord = require('./updateRecord/index')
const sumRecord = require('./sumRecord/index')
const selectSubject = require('./selectSubject/index')


// 云函数入口函数
exports.main = async (event, context) => {
  switch (event.type) {
    case 'getSwitch':
      return await getSwitch.main(event, context)
    case 'getImage':
      return await getImage.main(event, context)
    case 'createCollection':
      return await createCollection.main(event, context)
    case 'selectuserOrder':
      return await selectuserOrder.main(event, context)
    case 'updateRecord':
      return await updateRecord.main(event, context)
    case 'sumRecord':
      return await sumRecord.main(event, context)
    case 'selectSubject':
      return await selectSubject.main(event, context)
  }
}