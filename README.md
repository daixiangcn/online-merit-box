[![version](https://img.shields.io/badge/version-v1.0.0-brightgreen.svg)](https://gitee.com/daixiangcn/online-merit-box/)
[![star](https://gitee.com/daixiangcn/online-merit-box/badge/star.svg?theme=dark)](https://gitee.com/daixiangcn/online-merit-box)
[![fork](https://gitee.com/daixiangcn/online-merit-box/badge/fork.svg?theme=dark)](https://gitee.com/daixiangcn/online-merit-box)

# 效果图示

![输入图片说明](https://images.gitee.com/uploads/images/2021/0825/201355_bfe20aa6_1664731.jpeg "2021082501.jpg")


# 目录说明


```
Project
├─cloudfunctions            云函数文件夹
│  ├─payCallback            微信支付回调函数
│  ├─quickstartFunctions
│  │  ├─createCollection
│  │  ├─getMiniProgramCode
│  │  ├─getSwitch           获取显示开关函数
│  │  ├─selectSubject       获取项目列表函数
│  │  ├─selectuserOrder     获取捐赠信息函数
│  │  ├─sumRecord
│  │  └─updateRecord
│  ├─randomString           随机数生成函数
│  └─wxPay                  微信支付函数
└─miniprogram               小程序文件夹
    ├─components
    │  └─cloudTipModal
    ├─images                图片文件夹
    ├─pages
    │  ├─about              关于我们页面
    │  ├─index              首页
    │  └─message            支付结果展示页面
    └─style
```



# 云数据库
云开发提供了一个 JSON 数据库，顾名思义，数据库中的每条记录都是一个 JSON 格式的对象。一个数据库可以有多个集合（相当于关系型数据中的表），集合可看做一个 JSON 数组，数组中的每个对象就是一条记录，记录的格式是 JSON 对象。
关系型数据库和 JSON 数据库的概念对应关系如下表：
| 关系型          | 文档型             |
|--------------|-----------------|
| 数据库 database | 数据库 database    |
| 表 table      | 集合 collection   |
| 行 row        | 记录 record / doc |
| 列 column     | 字段 field        |

## subject 集合
````json
[
    {
        "_id":"cd045e75612194f707091f700767ecbc",
        "name":"点灯"
    },
    {
        "_id":"cd045e756121952b070926767fa36429",
        "name":"供僧"
    },
    {
        "_id":"14139e1261219536064d820d2e044a16",
        "name":"佛前大供花果"
    },
    {
        "_id":"2d44d6c2612195400654ac156db1ab4a",
        "name":"放生"
    },
    {
        "_id":"cd045e756121954a0709284830c688c5",
        "name":"精舍修缮"
    },
    {
        "_id":"cd045e7561219552070928b767221da6",
        "name":"供斋"
    },
    {
        "_id":"cd045e756121955c070929407e92cd21",
        "name":"供养三宝"
    }
]
````
## switch 集合
````json
[
    {
        "_id":"2d44d6c26122f06b0679c86719c1a25b",
        "name":"showinfo",
        "status":"1"
    }
]
````
## userOrder 集合
````json
[
    {
        "_id":"8937eaa9612461440605c249016f83d4",
        "openid":"oIn3a5EToVcbYy9BF7mjOOHpJTPQ",
        "status":1,
        "name":"爱心人士",
        "subject":"点灯",
        "date":1629774149,
        "totalFee":1,
        "orderid":"LYJS16297741486628b0b7349aa3",
        "subMchId":"1613296330",
        "timeEnd":"20210824110245"
    }
]
````

# 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

